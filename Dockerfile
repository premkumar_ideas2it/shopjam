FROM ruby:2.4.1
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /shopjam
WORKDIR /shopjam
ADD . /shopjam
RUN gem install bundler
RUN bundle install
RUN bundle exec rake db:create RAILS_ENV=development
RUN bundle exec rake db:migrate RAILS_ENV=development
ENTRYPOINT rails s -p 80 -b 0.0.0.0 -e development
