Rails.application.routes.draw do

  devise_for :users, controllers: {registrations: "users/registrations"}

  root 'profile#index'

  get 'coles'                       => 'profile#coles', as: :coles

  post 'coles'                      => 'profile#colesUpdate', as: :coles_update

  get "coles_list"                  => 'cole#coles_list', as: :coles_list

  get "coles_list/:list/products"   => 'cole#coles_list_product', as: :coles_list_product

  get "coles_order"                 => 'cole#coles_order', as: :coles_order

  get "coles_order/:order/products" => 'cole#coles_order_product', as: :coles_order_product

  get "get_coles_details"           => 'cole#get_coles_order_products', as: :getColesOrderProducts

  post "add-products-to-cart"           => 'cole#add_products_to_cart', as: :add_products_to_cart
end
