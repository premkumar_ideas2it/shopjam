require 'fog'
class SyncAudit < ActiveRecord::Base

	belongs_to :user

# callback to create a temporary file
	before_create :CreateS3JsonFile

	private
	# upload scraped data in  AWS s3 using fog
	def CreateS3JsonFile
		connection = Fog::Storage.new({ :provider => "AWS",
	  :aws_access_key_id => COLES_INFO["AWS"]["access_key"],
	  :aws_secret_access_key => COLES_INFO["AWS"]["secret_key"],
	  :region                 => COLES_INFO["AWS"]["region"]
	  })
		bucket = connection.directories.new(:key => COLES_INFO["AWS"]["s3"])
		randTxt = SecureRandom.hex(3)
		self.original_filename = "#{self.user.email[/[^@]+/]}-#{randTxt}.json"
		s3_file_object = bucket.files.create(:key => "#{self.user.email[/[^@]+/]}-#{randTxt}.json", :body => {'Data': self.data_scraped}.to_json, :content_type => "text/plain", :acl => "private")
	end
end
