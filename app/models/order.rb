class Order < ActiveRecord::Base
	belongs_to :user
  has_many :products, as: :item

	def self.get_coles_order user, cookie, store_id
		order_url = "#{COLES_INFO["lists"]}#{store_id}/order/@rshistory?pageNumber=1&pageSize=5&updateCookies=true"
		response = RestClient.get(order_url, {content_type: :json, cookie: cookie})
		return response
	end

	def self.save_coles_orders user, orders
		orders = JSON.parse(orders.body)
		orders["ordersList"].each do |order|
			if Order.find_by(OrderId: order["orderId"], user_id: user.id).blank?
				@item = Order.create( user_id: user.id, DeliveryDate: order["orderAttributesMap"]["DMDeliveryWindowEndTime"], OrderId: order["orderId"], ProductsCount: 0)
			else
				@item = Order.find_by( user_id: user.id, OrderId: order["orderId"])
				@item.update(ProductsCount: 0, DeliveryDate: order["orderAttributesMap"]["DMDeliveryWindowEndTime"]) unless @item.blank?
			end
    end
	end

	def self.get_coles_products user, orders, cookie, store_id, catelog_id
		orders["ordersList"].each do |order|
			order = user.orders.find_by(OrderId: order["orderId"])
			products = Product.get_coles_products(order, cookie, store_id, catelog_id)
			# user.sync_audits.create(data_scraped: [{products: products}].to_json, sync_type: "OrderProduct")
			Product.save_coles_order_products(products, order)
		end
	end

	def self.last_updated_at
		order = Order.order("updated_at desc")
		return order.blank? ? "Never" : order.first.updated_at.blank? ? "Never" : order.first.updated_at.strftime("%d %b %Y")
	end

	def getFormattedProducts store_id
		products = self.products
		records = Array.new
		products.each do |product|
			records << {"createDate" => product.updated_at.to_formatted_s(:db), "productId" => "#{product.Stockcode.to_i}", "quantity" => "#{product.quantity}"} unless product.price.nil?
		end
		return {"orderItem" => records,"x_originalStoreId" => store_id}
	end

end
