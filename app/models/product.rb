require 'nokogiri'
class Product < ActiveRecord::Base
	belongs_to :item, :polymorphic => true

	def self.get_coles_products item, cookie, store_id, catelog_id
		url = Product.get_coles_product_url(item, store_id, catelog_id)
		response = RestClient.get(url, {content_type: :json, cookie: cookie})
		return response
	end

	def self.save_coles_products product_response, item
		nokogiri_res = Nokogiri::HTML(product_response)
		filter_res = nokogiri_res.css("div[data-colrs-transformer='colrsExpandFilter']")[0].text
		prod_res_json = JSON.parse(filter_res)
		products = prod_res_json["products"]
		# products = JSON.parse(products.body)
		unless products.blank?
			products.each do |product|
				unless item.products.where(Stockcode: product['s9']).any?
					item.products.create(description: product['n'], price: product['p1']['o'], Stockcode: product['s9'], quantity: product['lD']['q'], QuantityInTrolley: product['lD']['q'])
				else
					@product = item.products.find_by(Stockcode: product['s9'])
					@product.update(description: product['n'], price: product['p1']['o'], Stockcode: product['s9'], quantity: product['lD']['q'], QuantityInTrolley: product['lD']['q']) unless @product.blank?
				end
			end
		end
	end

	def self.save_coles_order_products products, item
		# products = product_response.catalogEntryView
		products = JSON.parse(products.body)
		unless products['catalogEntryView'].blank?
			products['catalogEntryView'].each do |product|
				unless item.products.where(Stockcode: product['s9']).any?
					item.products.create(description: product['n'], price: product['o3']['p1'], Stockcode: product['s9'], quantity: product['o3']['q'], QuantityInTrolley: product['o3']['q'])
				else
					@product = item.products.find_by(Stockcode: product['s9'])
					@product.update(description: product['n'], price: product['o3']['p1'], Stockcode: product['s9'], quantity: product['o3']['q'], QuantityInTrolley: product['o3']['q']) unless @product.blank?
				end
			end
		end
	end

	def self.get_coles_product_url item, store_id, catelog_id
		unless item.blank?
			if item.class.name == "List"
				return "#{COLES_INFO["listproduct"]}#{store_id}&catalogId=#{catelog_id}&tabType=lists&wishlistId=#{item.listID}&personaliseSort=false&errorView=AjaxActionErrorResponse&requesttype=ajax&beginIndex=0"
			end
			if item.class.name == "Order"
				return "#{COLES_INFO["orderproduct"]}#{store_id}/order/#{item.OrderId}/items?updateCookies=true"
			end
		end
	end

end
