class List < ActiveRecord::Base
	belongs_to :user
  has_many :products, as: :item

	def self.get_coles_list user, cookie, store_id
		list_url = "#{COLES_INFO["lists"]}#{store_id}/colrswishlist/@self?responseFormat=json&updateCookies=true"
		response = RestClient.get(list_url, {content_type: :json, cookie: cookie})
		return response
	end

	def self.save_coles_list user, lists
		lists = JSON.parse(lists.body)
		lists["lists"].each do |list|
			if List.find_by(listID: list["wishlistId"], user_id: user.id).blank?
				@item = List.create( user_id: user.id, name: list["listName"], listID: list["wishlistId"], products_count: list["count"].to_i)
			else
				@item = List.find_by( user_id: user.id, listID: list["wishlistId"])
				@item.update(products_count: list["count"].to_i, name: list["listName"]) unless @item.blank?
			end
    end
	end

	def self.get_coles_products user, lists, cookie, store_id, catelog_id
		lists.each do |list|
			list = user.lists.find_by(listID: list["wishlistId"])
			product_response = Product.get_coles_products(list, cookie, store_id, catelog_id)
			# user.sync_audits.create(data_scraped: [{products: products}].to_json, sync_type: "ListProduct")
			Product.save_coles_products(product_response, list)
		end
	end

	def self.last_updated_at
		list = List.order("updated_at desc")
		list.blank? ? "Never" : list.first.updated_at.blank? ? "Never" : list.first.updated_at.strftime("%d %b %Y")
	end

	def getFormattedProducts store_id
		products = self.products
		records = Array.new
		products.each do |product|
			records << {"createDate" => product.updated_at.to_formatted_s(:db), "productId" => "#{product.Stockcode.to_i}", "quantity" => "#{product.quantity}"} unless product.price.nil?
		end
		return {"orderItem" => records,"x_originalStoreId" => store_id}
	end

end
