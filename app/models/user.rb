class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  validates :email, presence: true
  validates_format_of :email,:with => Devise.email_regexp

  has_many :lists
  has_many :sync_audits
  has_many :orders

  def self.validate_coles_creds uname, password
    home_response = RestClient.get(COLES_INFO["homepage"])
    uid = home_response.cookies['UID'].split("|")
    cookie_id = uid[0]
    store_id= uid[1]
    cookies = "WC_GENERIC_ACTIVITYDATA=#{home_response.cookies['WC_GENERIC_ACTIVITYDATA']}"
    login_url = "#{COLES_INFO["login"]}#{store_id}/loginidentity?updateCookies=true"
    login_response = begin RestClient.post(login_url,
                        {'email' => uname,
                        'logonPassword' => password, 'rememberMe' => 'false',
                        'storeFrontUser' => "true", "storeId" => store_id}.to_json,
                        {content_type: :json, cookie: cookies})
    rescue => e
      e.response
    end
    if login_response.code == 400
      return "Invalid"
    else
      login_res_json = JSON.parse(login_response)
      return login_res_json
    end
  end

  def validate_coles_account
    return User.validate_coles_creds(self.coles_username, Base64.decode64(self.coles_password))
  end

  def login_coles_account
    home_response = RestClient.get(COLES_INFO["homepage"])
    uid = home_response.cookies['UID'].split("|")
    cookie_id = uid[0]
    store_id= uid[1]
    cookies = "WC_GENERIC_ACTIVITYDATA=#{home_response.cookies['WC_GENERIC_ACTIVITYDATA']}"
    login_url = "#{COLES_INFO["login"]}#{store_id}/loginidentity?updateCookies=true"
    login_response = begin RestClient.post(login_url,
                        {'email' => self.coles_username,
                        'logonPassword' => Base64.decode64(self.coles_password), 'rememberMe' => 'false',
                        'storeFrontUser' => "true", "storeId" => store_id}.to_json,
                        {content_type: :json, cookie: cookies})
    rescue => e
      e.response
    end
    if login_response.code == 400
      cookies = ""
      return {cookie: cookies, status: "Invalid"}
    else
      login_res_json = JSON.parse(login_response)
      login_cookies = login_response.cookies
      return {cookie: login_cookies, status: login_res_json}
    end
  end

  def send_products_to_coles_cart cart, cookie, store_id
    cart_url = "#{COLES_INFO["addcart"]}#{store_id}/cart/@self/update_order_item?updateCookies=true"
    add_cart_resp = RestClient.put(cart_url,cart.to_json, {content_type: :json, cookie: cookie})
    return JSON.parse(add_cart_resp)
  end

end
