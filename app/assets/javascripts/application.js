// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require bootstrap.min.js
//= require_tree .


$(document).ready(function(){
	$.fn.SendColesListProducts = function(url) {
		$('#SendColesListProductsModel').modal('show');
		$('.modal-title').empty().append("<h4>Sending to coles.com</h4>");
		$('.modal-body-content').empty().append("<p>Sending products to shopping cart. It may take a few minutes, please wait.</p>");
		$('.model-actions').empty().append('<span><img src="/assets/loading.gif"/></span>');
		setTimeout(function(){
			$.ajax({
      		url: url,
      		type: 'post',
      		success: function(data) {
      			$('.modal-title').empty().append("<h4>Success! Your Coles Trolley is ready</h4>");
      			$('.modal-body-content').empty().append("<p>Products are waiting in your shopping cart. Please <a href='coles.com.au' target='_blank'>login to coles</a> to complete your order.</p>");
      			$('.model-actions').empty();
      		},
      		error: function() {
      			$('.modal-title').empty().append("<h4>Something went wrong. Try again</h4>");
      			$('.modal-body-content').empty().append("<p>Products could not be processed. Please try again.</p>");
      			$('.model-actions').empty().append('<button class="btn signin" onclick="$.fn.SendColesListProducts(\''+url+'\');">Send products to coles.com</button>');
      		}
      	});
		},1000);
	}

	$('.showpasswordBtn').click(function ()  {
		var type = $(".showpassword").prop("type");
		if(type == "password") {
			$(".showpassword").prop('type', 'text');
			$('.showpasswordBtn').text('Hide password');
		} else if(type == "text") {
			$(".showpassword").prop('type', 'password');
			$('.showpasswordBtn').text('Show password');
		}
	});

	$("form").submit(function() {
		if($('.showpassword').length>0) {
			$('.showpassword').prop('type', 'password');
		}
	});

	$.fn.getColesOrderProducts = function(ele,url) {
		$(ele).attr('disabled', 'true');
		$('.spinnerparent').css('opacity', '0.1');
		$('.spinner').append('<span><img src="/assets/loading.gif"/></span>').css('z-index', '1');
		window.location = url;
	}

	$.fn.addProductsToCart = function(url, id, type) {
		$('#addProductsToCartModal').modal('show');
		$('.modal-title').empty().append("<h4>Sending to coles.com</h4>");
		$('.modal-body-content').empty().append("<p>Sending products to Trolley. It may take a few minutes, please wait.</p>");
		$('.model-actions').empty().append('<span><img src="/assets/loading.gif"/></span>');
		setTimeout(function(){
			$.ajax({
					url: url,
					type: 'post',
					data: { id: id, type: type },
					success: function(data) {
						$('.modal-title').empty().append("<h4>Success! Your Coles Trolley is ready</h4>");
						$('.modal-body-content').empty().append("<p>Products are waiting in your shopping cart. Please <a href='https://shop.coles.com.au/a/a-national/everything/browse' target='_blank'>login to Coles</a> to complete your order</p>");
						$('.model-actions').empty();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						$('.modal-title').empty().append("<h4>Something went wrong. Try again</h4>");
						$('.modal-body-content').empty().append("<p>"+jqXHR.responseJSON.status+"</p>");
						$('.model-actions').empty();
						if(jqXHR.responseJSON.button != false) {
							$('.model-actions').empty().append('<button class="btn signin" onclick="$.fn.addProductsToCart(\''+url+'\', \''+id+'\', \''+type+'\');">Send products to Coles</button>');
						}
					}
				});
		},1000);
	}

})
