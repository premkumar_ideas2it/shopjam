class ProfileController < ApplicationController
  before_filter :authenticate_user!, except: [:index]

  def index
    redirect_to coles_list_path if current_user
  end

	def colesUpdate
		begin
			if(params[:user][:coles_username].blank?)
				flash[:error] = "Coles username can't be blank."
				redirect_to coles_path and return
			elsif params[:user][:coles_password].blank?
				flash[:error] = "Coles password can't be blank."
				redirect_to coles_path and return
			elsif params[:user][:coles_username] !~ (/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i)
        flash[:error] = "Provide valid coles username."
        redirect_to coles_path and return
      end
      unless User.validate_coles_creds(params[:user][:coles_username], params[:user][:coles_password]) == "Invalid"
        params[:user][:coles_password] = Base64.encode64(params[:user][:coles_password])
        current_user.update(coles_credebtials)
        flash[:success] = "Success! Your Coles account details have been verified"
      else
        flash[:error] = "Something went wrong. We couldn't verify your Coles account. Please check your details below and try again."
      end
      redirect_to coles_path
		rescue Exception => e
      puts e.backtrace
			flash[:error] = "Error occurred while updating coles login details."
			redirect_to coles_path
		end
	end

	private

	def coles_credebtials
		params.require(:user).permit(:coles_username, :coles_password)
	end
end
