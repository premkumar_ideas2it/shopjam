# Only authenticated users can access the controller
# Accessable for verified coles login info users
class ColeController < ApplicationController

	before_filter :authenticate_user!

	before_filter :checkColesCreds!

	# Action to display lists for the current user.
	def coles_list
		@lists = current_user.lists
	end

	# Action to display products for a particular list.
	def coles_list_product
		begin
			@list = current_user.lists.find_by(id: params[:list])
			if @list.blank?
				flash[:notice] = "Requested list not available."
				redirect_to coles_list_path and return
			end
		rescue Exception => e
			flash[:error] = "Something happend in retrieving products for the requested list."
			redirect_to coles_list_path
		end
	end

	def coles_order
		@orders = current_user.orders
	end

	def coles_order_product
		begin
			@order = current_user.orders.find_by(id: params[:order])
			if @order.blank?
				flash[:notice] = "Requested order not available."
				redirect_to coles_order_path and return
			end
		rescue Exception => e
			flash[:error] = "Something happend in retrieving products for the requested order."
			redirect_to coles_order_path
		end
	end

	# Get the list and order along with their products
	def get_coles_order_products
		begin
			cookie = ""
			login_resp = current_user.login_coles_account
			if login_resp[:status] == "Invalid"
				flash[:notice] = "Please verify and update your coles login details."
				redirect_to coles_path and return
			end
			cookie = login_resp[:cookie]
			status = login_resp[:status]
			uid = cookie['UID'].split("|")
			catelog_id = status['catalogId']
			cookie_id = uid[0]
			store_id = uid[1]
			past_order_cookies = ""
			past_order_cookies += "WC_AUTHENTICATION_#{cookie_id}=#{cookie['WC_AUTHENTICATION_'+cookie_id]};"
			past_order_cookies += "WC_USERACTIVITY_#{cookie_id}=#{cookie['WC_USERACTIVITY_'+cookie_id]};"
			past_order_cookies += "rslc=#{cookie['rslc']}"


			list_response = List.get_coles_list current_user, past_order_cookies, store_id
			List.save_coles_list current_user, list_response
			lists = JSON.parse(list_response.body)
			List.get_coles_products current_user, lists["lists"], past_order_cookies, store_id, catelog_id

			order_response = Order.get_coles_order current_user, past_order_cookies, store_id
			Order.save_coles_orders current_user, order_response
			orders = JSON.parse(order_response.body)
			Order.get_coles_products current_user, orders, past_order_cookies, store_id, catelog_id

			if request.referrer.blank?
				flash[:success] = "Your Coles lists and orders has been retrieved."
				redirect_to coles_list_path
			elsif request.referrer.include?(coles_list_path)
				flash[:success] = "Your Coles lists has been retrieved."
				redirect_to coles_list_path
			elsif request.referrer.include?(coles_order_path)
				flash[:success] = "Your Coles past orders has been synched."
				redirect_to coles_order_path
			else
				flash[:success] = "Your Coles lists and orders has been retrieved."
				redirect_to coles_list_path
			end

		rescue Exception => e
			puts e.backtrace
			flash[:error] = "Something's wrong. Your Coles lists has not retrieved."
			redirect_to coles_list_path
		end
	end

	def add_products_to_cart
		begin
			render :json => {status: "Please verify and update your <a href='#{coles_path}'>coles login</a> details.", button: true}, :status => 401 and return if current_user.validate_coles_account == "Invalid"
			if(params[:type] == "list")
				@item = List.find_by(id: params[:id])
				render :json => {status: "List not found to add its products to cart.", button: true}, :status => 404 and return if @item.blank?
			elsif(params[:type] == "order")
				@item = Order.find_by(id: params[:id])
				render :json => {status: "Order not found to add its product to cart.", button: true}, :status => 404 and return if @item.blank?
			else
				render :json => {status: "You can't do this."}, :status => 401 and return
			end
			cookie = ""
			login_resp = current_user.login_coles_account
			if login_resp[:status] == "Invalid"
				flash[:notice] = "Please verify and update your coles login details."
				redirect_to coles_path and return
			end
			cookie = login_resp[:cookie]
			status = login_resp[:status]
			uid = cookie['UID'].split("|")
			catelog_id = status['catalogId']
			cookie_id = uid[0]
			store_id = uid[1]
			past_order_cookies = ""
			past_order_cookies += "WC_AUTHENTICATION_#{cookie_id}=#{cookie['WC_AUTHENTICATION_'+cookie_id]};"
			past_order_cookies += "WC_USERACTIVITY_#{cookie_id}=#{cookie['WC_USERACTIVITY_'+cookie_id]};"
			past_order_cookies += "rslc=#{cookie['rslc']}"
			products = Product.get_coles_products @item, past_order_cookies, store_id,catelog_id
			if(params[:type] == "list")
				Product.save_coles_products products, @item
			elsif(params[:type] == "order")
				Product.save_coles_order_products products, @item
			else
				Product.save_coles_products products, @item
			end
			cart = @item.getFormattedProducts store_id
			cart_response = current_user.send_products_to_coles_cart cart, past_order_cookies, store_id
			# current_user.sync_audits.create(data_scraped: [{products: cart_response}].to_json, sync_type: "Cart")
			# current_user.woolworths_logout cookie
			render :json => {status: "Success", button: true}, :status => 200 and return
		rescue Exception => e
			render :json => {status: "Products could not be processed. Please try again.", button: true}, :status => 400 and return
		end
	end

end
