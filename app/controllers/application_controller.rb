class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def checkColesCreds!
    if current_user && (current_user.coles_username.blank? || current_user.coles_password.blank? )
      flash[:notice] = "You need to update your Coles credentials."
      redirect_to coles_path and return
    end
  end

  def after_sign_in_path_for(resource)
    flash[:notice] = "Signed in successfully."
    return coles_path
  end
end
