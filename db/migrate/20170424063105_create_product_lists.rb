class CreateProductLists < ActiveRecord::Migration
  def change
    create_table :product_lists, id: false do |t|
    	t.integer :list_id
    	t.integer :product_id
    end
  end
end
