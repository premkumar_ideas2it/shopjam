class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.text :description
    	t.integer :quantity
    	t.float :price
      t.string :Stockcode
      t.timestamps null: false
    end
  end
end
