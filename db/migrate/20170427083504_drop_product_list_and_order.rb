class DropProductListAndOrder < ActiveRecord::Migration
  def change
    drop_table :product_lists
    drop_table :product_orders
  end
end
