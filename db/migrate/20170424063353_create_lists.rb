class CreateLists < ActiveRecord::Migration
  def change
    create_table :lists do |t|
      t.integer :user_id
    	t.string :name
      t.string :listID
    	t.integer :products_count
      t.timestamps null: false
    end
  end
end
