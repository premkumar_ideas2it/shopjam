class CreateProductOrders < ActiveRecord::Migration
  def change
    create_table :product_orders, id: false do |t|
      t.integer :order_id
    	t.integer :product_id
    end
  end
end
