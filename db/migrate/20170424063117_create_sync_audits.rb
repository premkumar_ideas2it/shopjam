class CreateSyncAudits < ActiveRecord::Migration
  def change
    create_table :sync_audits do |t|
      t.integer :user_id
    	t.json :data_scraped
    	t.string :sync_type
    	t.string :status_code
    	t.string :http_url
    	t.string :http_method
    	t.text :response_header
      t.string :url
      t.string :original_filename
    	t.timestamps null: false
    end
  end
end
